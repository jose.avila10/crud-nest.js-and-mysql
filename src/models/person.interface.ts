export interface Person {
    id: number,
    name: string,
    lastname: string,
    adress: string,
    city: string
}
