import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity('persons')
export class PersonEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    lastname: string;

    @Column()
    adress: string;

    @Column()
    city: string;

}
