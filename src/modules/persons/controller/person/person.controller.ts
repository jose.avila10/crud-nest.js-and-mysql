import { Controller, Get, Post, Put, Delete, Param, Body } from '@nestjs/common';
import { PersonService } from 'src/modules/persons/services/person/person.service';
import { Person } from 'src/models/person.interface';

@Controller('persons')
export class PersonController {
    constructor(private personServices: PersonService) {
    }

    @Post()
    addPerson(@Body() personModel: Person): any {
        return this.personServices.savePerson(personModel);
    }

    @Get()
    getPerson(): any {
        return this.personServices.findAll();
    }

    @Get(':id')
    getOnePerson(@Param() params): any {
        return this.personServices.findOne(params.id);
    }

    @Put(':id')
    updatePerson(@Body() personsModel: Person, @Param() params): any {
        return this.personServices.updatePerson(params.id, personsModel);
    }

    @Delete(':id')
    deletePerson(@Param() params): any {
        return this.personServices.deletePerson(params.id);
    }
}

/*
Headers:
KEY: Content-Type, VALUE: application/json
Body:
{
    "name":"John",
    "lastname":"Doe",
    "adress":"98036 Leffingwell Av.",
    "city":"Whittier"
}
*/
