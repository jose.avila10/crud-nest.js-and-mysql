import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PersonEntity } from 'src/entity/person-entity';

@Injectable()
export class PersonService {
    constructor(
        @InjectRepository(PersonEntity)
        private readonly personRP: Repository<PersonEntity>
    ) {

    }

    async savePerson(person: any) {
        await this.personRP.insert(person);
        return person
    }

    async updatePerson(id: number, person: any) {
        await this.personRP.update(id, person);
    }

    async findAll() {
        return await this.personRP.find();
    }

    async findOne(id: number) {
        return await this.personRP.findOne(id);
    }

    async deletePerson(id: number) {
        return await this.personRP.delete(id);
    }
}
