import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonService } from './services/person/person.service';
import { PersonController } from './controller/person/person.controller';
import { PersonEntity } from 'src/entity/person-entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([PersonEntity])
  ],

  providers: [PersonService],

  controllers: [PersonController]
})
export class PersonsModule { }
