import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { PersonsModule } from '../persons/persons.module';
import { PersonEntity } from 'src/entity/person-entity';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'root',
            password: 'FlashDrive27@linux!',
            database: 'test_db',
            entities: [
                PersonEntity
            ],
            synchronize: true
        }),
        PersonsModule
    ]
})
export class DatabaseModule {
    constructor(private readonly connection: Connection) {

    }
}
